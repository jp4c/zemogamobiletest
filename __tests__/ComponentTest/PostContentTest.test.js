import 'react-native';
import React from 'react';

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';
import { Provider } from 'react-redux';
import PostContent from '../../src/components/navigation/PostContent';
import rootReducer from '../../src/redux/store'
import { createStore } from 'redux';
jest.useFakeTimers();
describe('POST CONTENT COMPONENT TEST', () => {
  
  it('renders correctly', () => {
    const store = createStore(rootReducer)
    const navigation = { navigate: jest.fn() };
    const tree = renderer.create(
      <Provider store={store} >
        <PostContent navigation={navigation} />
      </Provider>
    ).toJSON();
    expect(tree).toMatchSnapshot();
  })

})