import 'react-native';
import React from 'react';

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';
import { Provider } from 'react-redux';
import UserInfo from '../../src/components/User/UserInfo';
import rootReducer from '../../src/redux/store'
import { createStore } from 'redux';
jest.useFakeTimers();
describe('FAVORITES COMPONENT TEST', () => {
  
  it('renders correctly', () => {
    const store = createStore(rootReducer)
    const navigation = { navigate: jest.fn() };
    const tree = renderer.create(
      <Provider store={store} >
        <UserInfo navigation={navigation} />
      </Provider>
    ).toJSON();
    expect(tree).toMatchSnapshot();
  })

})