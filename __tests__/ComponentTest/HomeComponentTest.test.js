/**
 * @format
 */

import 'react-native';
import React from 'react';
import App from '../../App';

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';
import { Provider } from 'react-redux';
import HomeScreen from '../../src/components/navigation/Home';
import rootReducer from '../../src/redux/store'
import { createStore } from 'redux';
jest.useFakeTimers();
describe('HOME COMPONENT TEST', () => {
  
  it('renders correctly', () => {

    const store = createStore(rootReducer)

    const navigation = { navigate: jest.fn() };
    const tree = renderer.create(
      <Provider store={store} >
        <HomeScreen navigation={navigation} />
      </Provider>
    ).toJSON();
    expect(tree).toMatchSnapshot();
  })

})
