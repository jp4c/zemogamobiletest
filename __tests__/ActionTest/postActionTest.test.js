import 'react-native';

import * as postAction from '../../src/redux/actions/postAction'
jest.useFakeTimers();
describe('POST ACTIONS TEST', () => {
  
  it('should create an action to load_posts', () => {

    const posts =
      [
        {
          "userId": 2,
          "id": 1,
          "title": "Sed ut perspiciatis unde omnis iste natus error sit voluptatem",
          "body": "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.",
          "isRead": false,
          "isFavorite": false
        },
        {
          "userId": 3,
          "id": 2,
          "title": "Sed ut perspiciatis unde omnis iste natus error sit voluptatem",
          "body": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
          "isRead": false,
          "isFavorite": false
        },
        {
          "userId": 1,
          "id": 3,
          "title": "At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas",
          "body": "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.",
          "isRead": false,
          "isFavorite": false
        }
      ]

    const expectedAction = {
      type: 'load_posts',
      posts
    }
    expect(postAction.load_posts(posts)).toEqual(expectedAction)
  })
  it('should create an action to load_original_posts', () => {

    const posts =
      [
        {
          "userId": 2,
          "id": 1,
          "title": "Sed ut perspiciatis unde omnis iste natus error sit voluptatem",
          "body": "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.",
          "isRead": false,
          "isFavorite": false
        },
        {
          "userId": 3,
          "id": 2,
          "title": "Sed ut perspiciatis unde omnis iste natus error sit voluptatem",
          "body": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
          "isRead": false,
          "isFavorite": false
        },
        {
          "userId": 1,
          "id": 3,
          "title": "At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas",
          "body": "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.",
          "isRead": false,
          "isFavorite": false
        }
      ]

    const expectedAction = {
      type: 'load_original_posts',
      posts
    }
    expect(postAction.load_original_posts(posts)).toEqual(expectedAction)
  })
  it('should create an action to reload_posts', () => {
    const expectedAction = {
      type: 'reload_posts'
    }
    expect(postAction.reload_posts()).toEqual(expectedAction)
  })
  it('should not create an action for reload_posts', () => {
    const post = {
      "userId": 2,
      "id": 1,
      "title": "Sed ut perspiciatis unde omnis iste natus error sit voluptatem",
      "body": "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.",
      "isRead": false,
      "isFavorite": false
    }
    const expectedAction = {
      type: 'reload_posts'
    }
    expect(postAction.reload_posts(post)).toEqual(expectedAction)
  })
  it('should create an action to delete_post', () => {
    const id = 2
    const expectedAction = {
      type: 'delete_post',
      id
    }
    expect(postAction.delete_post(id)).toEqual(expectedAction)
  })
  it('should not create an action for reload_posts', () => {
    const post = {
      "userId": 2,
      "id": 1,
      "title": "Sed ut perspiciatis unde omnis iste natus error sit voluptatem",
      "body": "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.",
      "isRead": false,
      "isFavorite": false
    }
    const expectedAction = {
      type: 'reload_posts'
    }
    expect(postAction.reload_posts(post)).toEqual(expectedAction)
  })
  it('should create an action to delete_all_posts', () => {
    
    const expectedAction = {
      type: 'delete_all_posts'
    }
    expect(postAction.delete_all_posts()).toEqual(expectedAction)
  })
})