# **Final Result (Brief)**

![mobile_test](/uploads/6f1bffee6a01acf9b624c3b671c90c6e/mobile_test.mp4)


# **Run app**

When cloning or downloading the project, we must go to the folder where the project is located and install the dependencies, writing:

`npm install`

To run the application writing:

**for Android:**

`react-native run-android ` 

**for IOs**

`react-native run-ios ` 

# **App running ERROR**

**It is clarified that these problems are from React Native version (0.59)**

## **IOS Emulator**

![Captura_de_Pantalla_2020-06-10_a_la_s__5.22.35_p._m.](/uploads/002d3887c013dfc5af91072db0f03817/Captura_de_Pantalla_2020-06-10_a_la_s__5.22.35_p._m..png)

If this error appears in the ios emulator, it is fixed like this:

1) go to `node_modules/react-native/React/Base/RCTModuleMethod.mm`
2) add this: `RCTReadString(input, "__attribute__((__unused__))") ||`  , as the picture shows:

![Captura_de_Pantalla_2020-06-09_a_la_s__5.51.49_p._m.](/uploads/a3f22c51b4536cc7f88dd9d0642f8019/Captura_de_Pantalla_2020-06-09_a_la_s__5.51.49_p._m..png)

3) run the app again: `react-native run-ios ` 
4) Done!

## **Running app to IOS and Android Emulator**

![Captura_de_Pantalla_2020-06-10_a_la_s__4.28.58_p._m.](/uploads/a010bfbe865f81728680472052a2724f/Captura_de_Pantalla_2020-06-10_a_la_s__4.28.58_p._m..png)

![Captura_de_Pantalla_2020-06-10_a_la_s__4.29.10_p._m.](/uploads/b815559c7722a1c2b6310b9e354277c0/Captura_de_Pantalla_2020-06-10_a_la_s__4.29.10_p._m..png)


**If when running the app this error appears, either in the Android emulator and in the metro bundle. End Metro bundle and being in the project folder, place:**

 1) `npm start -- --reset-cache`
 2) enter.
 
 Done! Reload app and Metro bundler loads the app.
 
 
# **The proposed architecture**
 
 I used an MVC architecture mixed with the Redux library architecture.
 
 **Now why mixed?**
 
 As we know, with the MVC architecture I am dividing or modularizing what is the view and the functions that manipulate or obtain the data, where, with the controller I am making the requests to obtain the information of the API JSONPlaceholder and functions, as I said previously , that manipulate or change that data.
With Redux I am storing the response of the request, which are the posts, to have a single state in the entire application and to easily display the data in the different views.


# **Third-party library used**

## **Redux**

Redux is a predictable state container for JavaScript apps.

It helps to write applications that behave consistently, run in different environments (client, server, and native), and are easy to test. On top of that, it provides a great developer experience, such as live code editing combined with a time traveling debugger.

Redux can be used together with React, or with any other view library. It is tiny (2kB, including dependencies), but has a large ecosystem of addons available.

## **Redux-Persist**

Redux Persist takes the Redux state object and saves it to persisted storage. Then on app launch it retrieves this persisted state and saves it back to redux.

## **React-Navigation**

Mobile apps are rarely made up of a single screen. Managing the presentation of, and transition between, multiple screens is typically handled by what is known as a navigator.
React Navigation provides a straightforward navigation solution, with the ability to present common stack navigation and tabbed navigation patterns on both Android and iOS.
The community solution to navigation is a standalone library that allows developers to set up the screens of an app with a few lines of code.

## **Jest**

Jest is a JavaScript test runner, that is, a JavaScript library for creating, running, and structuring tests.

Jest ships as an NPM package, it can install it in any JavaScript project. Jest is one of the most popular test runner these days, and the default choice for React projects.

# **Running unit-tests with Jest**
we must go to the folder where the project is located and writing:

1) `npm run test`
2) enter

It shows the tests performed and also shows the snapshot rendering of the tested components, stored in rootProject/__tests __/ComponentTest/snapshot
 
