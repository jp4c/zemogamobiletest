export default class Post {

  constructor(userId, id, title, body) {
    this.userId = userId
    this.id = id
    this.title = title
    this.body = body
    this.isRead = true
    this.isFavorite = false
  }
  
  setFavorite(favorite) {
    this.isFavorite = favorite
  }

  setRead(read) {
    this.isRead = read
  }
}