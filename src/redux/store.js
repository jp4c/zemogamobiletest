import { combineReducers, } from 'redux';
import postReducer from './reducers/postReducer'

const rootReducer = combineReducers({
  favoritesPosts: postReducer
})

export default rootReducer;