export const set_read_post = id => {
  return {
    type: 'set_read_post',
    id
  }
}

export const load_posts = posts => {
  return {
    type: 'load_posts',
    posts
  }
}

export const load_original_posts = (posts) => {
  return {
    type: 'load_original_posts',
    posts
  }
}

export const reload_posts = () => {
  return {
    type: 'reload_posts'
  }
}

export const delete_post = id => {
  return {
    type: 'delete_post',
    id
  }
}

export const delete_all_posts = () => {
  return {
    type: 'delete_all_posts'
  }
}




