export const load_favorites_list = favorites_posts => {
  return {
    type: 'load_favorites_list',
    favorites_posts
  }
}

export const load_favorites_posts_ids = (favorites_posts_ids, posts) => {
  return {
    type: 'load_favorites_posts_ids',
    favorites_posts_ids,
    posts
  }
}

export const delete_favorite_post = id => {
  return {
    type: 'delete_favorite_post',
    id
  }
}
