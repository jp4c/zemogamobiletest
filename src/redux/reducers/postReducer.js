import PostController from "../../controllers/PostController"

const initialState = {
  originalPosts: [],
  posts: [],
  favoritePosts: [],
  favoritePostsIds: [],
  selected: false
}

function getFavoritesPosts(state, favoritesPosts) {
  postcontroller = new PostController()
  postcontroller.posts = [...state.posts]
  postcontroller.favoritesPosts = [...favoritesPosts]
  console.log("favorites ids reducer " + JSON.stringify(postcontroller.favoritesPosts))
  console.log("favorites information reducer " + JSON.stringify(postcontroller.getFavoritePostsInformation()))
  return postcontroller.getFavoritePostsInformation()
}

function setReadPost(state, id) {
  postcontroller = new PostController()
  postcontroller.posts = [...state.posts]
  postcontroller.setReadPost(id)
  return postcontroller.posts
}

function deletePost(state, id) {
  postcontroller = new PostController()
  postcontroller.posts = [...state.posts]
  postcontroller.deletePost(id)
  return postcontroller.posts
}

function deleteFavoritePost(state, id) {
  postcontroller = new PostController()
  postcontroller.posts = [...state.posts]
  postcontroller.favoritesPosts = [...state.favoritePostsIds]
  postcontroller.deletePost(id)
  postcontroller.deleteFavoritePost(id)
  return postcontroller.getFavoritesPosts()
}

function deleteFavoritePostId(state, id) {
  postcontroller = new PostController()
  postcontroller.favoritesPosts = [...state.favoritePostsIds]
  console.log('antes de eliminar ' + JSON.stringify(postcontroller.favoritesPosts))
  postcontroller.deleteFavoritePost(id)
  console.log('despues de eliminar ' + JSON.stringify(postcontroller.favoritesPosts))
  return postcontroller.favoritesPosts
}

export default (state = initialState, action) => {
  switch (action.type) {
    case 'load_favorites_list':
      return {
        ...state,
        favoritePosts: action.favorites_posts
      }
    case 'load_favorites_posts_ids':
      console.log("datos que llegan " + JSON.stringify(action.favorites_posts_ids))
      console.log("datos que hay en el store " + JSON.stringify(state.favoritePostsIds))
      return {
        ...state,
        favoritePosts: [...getFavoritesPosts(state, action.favorites_posts_ids)],
        favoritePostsIds: [...action.favorites_posts_ids]
      }
    case 'set_read_post':
      console.log('original ' + JSON.stringify(state.originalPosts))
      return {
        ...state,
        posts: [...setReadPost(state, action.id)]
      }
    case 'load_posts':
      return {
        ...state,
        posts: [...action.posts],
        selected: !state.selected
      }
    case 'load_original_posts':
      return {
        ...state,
        originalPosts: JSON.parse(JSON.stringify(action.posts)),
      }
    case 'reload_posts':
      console.log('original ' + JSON.stringify(state.originalPosts))
      return {
        ...state,
        posts: JSON.parse(JSON.stringify(state.originalPosts)),
        favoritePosts: [],
        favoritePostsIds: [],
        selected: !state.selected
      }
    case 'delete_post':
      return {
        ...state,
        posts: [...deletePost(state, action.id)]
      }
    case 'delete_favorite_post':
      return {
        ...state,
        posts: [...deletePost(state, action.id)],
        favoritePosts: [...deleteFavoritePost(state, action.id)],
        favoritePostsIds: [...deleteFavoritePostId(state, action.id)]
      }
    case 'delete_all_posts':
      return {
        ...state,
        posts: [],
        favoritePosts: [],
        favoritePostsIds: [],
        selected: !state.selected
      }
    default:
      return state
  }
}
