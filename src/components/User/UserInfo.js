import React, { Component } from "react"
import {
  Text,
  View,
  StyleSheet,
  ActivityIndicator
} from 'react-native';
import UserController from "../../controllers/UserController";
import Error from "../Error/Error";

export default class UserInfo extends Component {

  constructor(props) {
    super(props)
    this.userController = new UserController();
    this.state = {
      success: true,
    }
  }

  componentDidMount = async () => {
    const { user, success } = await this.userController.getOneUser(this.props.userId)
    this.setState({ user, success })
  }

  render() {
    return (
      <View>
        {
          this.state.user ?
            <View>
              <View style={styles.attributeContainer}>
                <Text style={styles.title}>Name:</Text>
                <Text style={styles.data}>{this.state.user.name}</Text>
              </View>
              <View style={styles.attributeContainer}>
                <Text style={styles.title}>Email:</Text>
                <Text style={styles.data}>{this.state.user.email}</Text>
              </View>
              <View style={styles.attributeContainer}>
                <Text style={styles.title}>Phone:</Text>
                <Text style={styles.data}>{this.state.user.phone}</Text>
              </View>
              <View style={styles.attributeContainer}>
                <Text style={styles.title}>Website:</Text>
                <Text style={styles.data}>{this.state.user.website}</Text>
              </View>
            </View>
            :
            !this.state.success ?
              <Error></Error>
              :
              <ActivityIndicator size="large" color="green"></ActivityIndicator>
        }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  attributeContainer: {
    flexDirection: 'row',
    marginTop: 10
  },
  title: {
    fontWeight: 'bold'
  },
  data: {
    marginStart: 10
  }
});