import React, { Component } from 'react'
import { View, Text, StyleSheet } from "react-native";
import Icon from 'react-native-vector-icons/dist/Entypo';

export default class Error extends Component {
  render() {
    return (
      <View style={styles.errorContainer}>
        <Icon
          name="emoji-sad"
          size={40} />
        <Text>Something went wrong, try later.</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  errorContainer: {
    marginTop: 10,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  }
})