import React, { Component } from "react";
import { View, Text, StyleSheet, ScrollView, FlatList, ActivityIndicator } from "react-native";
import CommentRow from "./CommentRow";
import CommentController from "../../controllers/CommentController";
import Error from "../Error/Error";


export default class Comments extends Component {

  constructor(props) {
    super(props)
    this.state = {
      success: true
    }
  }

  componentDidMount = async () => {
    commentController = new CommentController();
    const { success, comments } = await commentController.getComments(this.props.postId)
    this.setState({ success, comments })
  }

  renderRow = ({ item }) => {
    return (
      <CommentRow
        item={item}>
      </CommentRow>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.titleContainer}>
          <Text style={styles.title}>COMMENTS</Text>
        </View>
        {
          this.state.comments ?
            <ScrollView
              contentContainerStyle={styles.commentsContainer}>
              <View >
                <FlatList
                  data={this.state.comments}
                  renderItem={this.renderRow}
                  keyExtractor={item => "" + item.id} />
              </View>
            </ScrollView>
            :
            !this.state.success ?
              <Error></Error>
              :
              <ActivityIndicator size="large" color="green"></ActivityIndicator>
        }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 20,
    flexDirection: 'column'
  },
  titleContainer: {
    paddingStart: 15,
    height: 30,
    backgroundColor: 'lightgray',
    justifyContent: 'center',
  },
  title: {
    fontSize: 15,
    fontWeight: 'bold',
    textAlign: "left",
    textAlignVertical: "center"
  },
  commentsContainer:
  {
    flex: 1
  }
})