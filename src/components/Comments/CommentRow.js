import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";

export default class CommentRow extends Component {

  constructor(props) {
    super(props)
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>{this.props.item.body}</Text>
        <View style={styles.separator}></View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 10
  },
  separator: {
    marginTop: 15,
    backgroundColor: 'lightgray',
    width: this.width,
    height: 2,
  },
})
