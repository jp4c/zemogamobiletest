import React, { Component } from 'react';
import {
  Text,
  StyleSheet,
  TouchableHighlight,
  View,
  ScrollView,
  Dimensions
} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';

export default class Row extends Component {

  constructor(props) {
    super(props);
    this.width = Dimensions.get('window').width; //full width
  }

  onPressPost = id => {
    this.scrollView.scrollTo({ x: 0, animated: true })
    const { onPress } = this.props;
    onPress(id)
  }

  onPressPostDelete = id => {
    const { onPressDelete } = this.props;
    onPressDelete(id)
  }

  render() {
    const { item } = this.props;
    return (
      <ScrollView
        ref={ref => this.scrollView = ref}
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        directionalLockEnabled={false} >
        <TouchableHighlight
          onPress={() => { this.onPressPost(item.id) }} >
          <View style={{ width: this.width }}>
            <View style={styles.rowContainer}>
              <View style={styles.row}>
                {
                  !item.isRead &&
                  <View style={styles.circle}>
                  </View>
                }
                {
                  item.isFavorite &&
                  <Icon
                    name="star"
                    size={20}
                    color="gold"
                  />
                }
              </View>
              <View style={styles.postContainer} >
                <Text style={styles.postTitleText}> {item.title} </Text>
                <Text style={styles.postBodyText}> {item.body} </Text>
              </View>
              <View style={styles.endRow}>
                <Text style={{ fontSize: 18 }}> > </Text>
              </View>
            </View>
            <View style={styles.separator}></View>
          </View>
        </TouchableHighlight>
        <TouchableHighlight
          onPress={() => { this.onPressPostDelete(item.id) }} >
          <View style={styles.deleteContainer}>
            <Text style={styles.deleteText} >Delete</Text>
          </View>
        </TouchableHighlight>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  rowContainer: {
    paddingTop: 10,
    paddingBottom: 10,
    flex: 1,
    flexDirection: 'row'
  },
  row: {
    width: '10%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  endRow: {
    width: '10%',
    justifyContent: 'center',
    alignItems: 'flex-end',
    paddingEnd: 5
  },
  postContainer: {
    paddingEnd: 20,
    width: '75%'
  },
  postTitleText: {
    fontSize: 19,
    fontWeight: 'bold'
  },
  postBodyText: {
    marginTop: 15,
    fontSize: 19,
  },
  circle: {

    width: 13,
    height: 13,
    borderRadius: 44 / 2,
    backgroundColor: 'blue'
  },
  separator: {
    marginTop: 5,
    backgroundColor: 'lightgray',
    width: this.width,
    height: 2,
  },
  deleteContainer: {
    width: 80,
    flex: 1,
    backgroundColor: 'red',
    justifyContent: 'center'
  },
  deleteText: {
    color: 'white',
    fontSize: 17,
    textAlign: 'center',
    fontWeight: 'bold'
  }
});

