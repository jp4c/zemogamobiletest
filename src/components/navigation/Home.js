/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';

import {
  StyleSheet,
  Dimensions,
  View,
  ScrollView,
  FlatList,
  StatusBar,
  Text,
  TouchableOpacity,
  Alert,
  ActionSheetIOS,
  Platform
} from 'react-native';

import Row from '../Post/Row';

import PostController from '../../controllers/PostController';
import { connect } from 'react-redux';

import {
  set_read_post,
  load_posts,
  load_original_posts,
  delete_post,
  delete_all_posts
} from '../../redux/actions/postAction'

import {
  load_favorites_list,
  load_favorites_posts_ids,
  delete_favorite_post
} from '../../redux/actions/favoritePostsAction'

class Home extends Component {

  static navigationOptions = {
    title: "All",
    headerTitleStyle: { color: 'white', },
  };

  constructor(props) {
    super(props);
    this.width = Dimensions.get('window').width; //full width
    //var height = Dimensions.get('window').height; //full height
    this.postController = new PostController();

    this.state = {
      posts: [],
      selected: false
    }
  }

  componentDidMount = async () => {
    if (this.props.posts.length == 0) {
      const posts = await this.postController.getPostsFromCloud()
      this.props.loadPosts(posts)
      this.props.loadOriginalPosts(posts)
      this.setState({ posts })
    }
  };


  onPressItem = id => {
    this.props.setReadPost(id)
    this.postController.posts = this.props.posts
    this.props.navigation.navigate('PostContent',
      {
        postId: id,
        isFavorite: post.isFavorite,
        postController: this.postController,
        onPressFavorite: this.onPressFavorite
      })
  }

  onPressFavorite = (id) => {
    this.postController.favoritesPosts = this.props.favoritesPostsIds
    this.postController.setFavoritePost(id);
    console.log("lo que tiene controller home " + JSON.stringify(this.postController.favoritesPosts))
    this.props.loadPosts(this.postController.posts)
    this.props.setFavoritesPostsIds(this.postController.favoritesPosts, this.postController.posts)
  }

  onPressDelete = id => {
    const post = this.postController.getPost(id)
    if (post.isFavorite) {
      this.props.deleteFavoritePost(id)
    } else {
      this.props.deletePost(id)
    }
  }

  renderRow = ({ item, index }) => {
    return (
      <Row
        item={item}
        onPress={this.onPressItem}
        onPressDelete={this.onPressDelete}></Row>
    );
  }

  onPressDeleteAll = () => {
    if (Platform.OS === 'ios') {
      ActionSheetIOS.showActionSheetWithOptions(
        {
          message: 'Do you want to delete all posts?',
          options: ['Cancel', 'Delete all'],
          destructiveButtonIndex: 1,
          cancelButtonIndex: 0
        },
        (buttonIndex) => {
          if (buttonIndex === 1) {
            this.props.deleteAllPost()
          }
        }
      );
    }
    else {
      Alert.alert(
        null,
        'Do you want to delete all posts?',
        [
          {
            text: 'Delete',
            onPress: () => this.props.deleteAllPost()
          },
          {
            text: 'Cancel'
          },

        ]
      );
    }
  }

  render() {
    return (
      <View>
        <ScrollView>
          <StatusBar backgroundColor="green" />
          <View style={styles.container}>
            <FlatList
              data={this.props.posts}
              renderItem={this.renderRow}
              keyExtractor={item => "" + item.id}
              extraData={this.props} />
          </View>
        </ScrollView>
        <TouchableOpacity style={styles.bottom} onPress={() => this.onPressDeleteAll()}>
          <Text
            style={styles.deleteButton}>
            Delete All</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  deleteButton: {
    fontSize: 20,
    color: 'white',
  },
  bottom: {
    flex: 1,
    width: '100%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 0,
    backgroundColor: 'red',
  }
});

const mapStateToProps = (state) => {
  //alert('Home, entra del stores '+JSON.stringify(state.favoritesPosts.posts))
  return {
    selected: state.favoritesPosts.selected,
    posts: state.favoritesPosts.posts,
    favoritesPostsIds: state.favoritesPosts.favoritePostsIds,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setReadPost: (id) => dispatch(set_read_post(id)),
    setFavoritesPosts: (favorites) => dispatch(load_favorites_list(favorites)),
    loadPosts: (posts) => dispatch(load_posts(posts)),
    loadOriginalPosts: (posts) => dispatch(load_original_posts(posts)),
    setFavoritesPostsIds: (favorites, posts) => dispatch(load_favorites_posts_ids(favorites)),
    deletePost: (id) => dispatch(delete_post(id)),
    deleteFavoritePost: (id) => dispatch(delete_favorite_post(id)),
    deleteAllPost: () => dispatch(delete_all_posts())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)
