import React, { Component } from "react";
import { View, Text, StyleSheet, Alert } from "react-native";
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/dist/AntDesign';
import {
  reload_posts
} from '../../../redux/actions/postAction'

class HeaderBar extends Component {

  onPressReloadPosts = () => {
    this.props.reloadPosts()
    Alert.alert(
      null,
      'Reload posts!'
    );
  }

  render() {
    return (
      <View style={styles.headerContainer}>
        <Text style={styles.title}>Posts</Text>
        <View style={styles.iconContainer}>
          <Icon
            style={styles.icon}
            name="reload1"
            size={20}
            onPress={() => { this.onPressReloadPosts() }}
            color="white"
          />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  headerContainer: {
    flex: 1,
    flexDirection: "row",
  },
  title: {
    flex: 1,
    marginStart: 20,
    textAlign: 'center',
    color: 'white',
    fontSize: 18,
  },
  icon: {
    justifyContent: 'flex-end'
  },
  iconContainer: {
    justifyContent: 'flex-end',
    paddingEnd: 10
  }
});

const mapDispatchToProps = (dispatch) => {
  return {
    reloadPosts: () => dispatch(reload_posts()),
  }
}

export default connect(null, mapDispatchToProps)(HeaderBar)
