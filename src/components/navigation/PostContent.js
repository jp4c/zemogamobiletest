import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  ActivityIndicator
} from 'react-native';
import PostController from '../../controllers/PostController';
import UserInfo from '../User/UserInfo';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import Comments from '../Comments/Comments';

export default class Favorites extends Component {

  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: null,
      title: "Post",
      headerTintColor: 'white',
      headerRightContainerStyle: { marginRight: 10 },
      headerRight: (
        <View>
          {navigation.getParam('isFavorite') ?
            <Icon
              name="star"
              size={30}
              onPress={() => Favorites.onPressStar(navigation.getParam('postId'), navigation)}
              color="#fff"
            />
            :
            <Icon
              name="star-o"
              size={30}
              onPress={() => Favorites.onPressStar(navigation.getParam('postId'), navigation)}
              color="#fff"
            />
          }
        </View>
      ),
    };
  };

  constructor(props) {
    super(props)
    this.postController = new PostController();
    this.state = {
      post: [],
      id: -1
    }
  }

  componentDidMount = async () => {
    const { navigation } = this.props;
    const id = navigation.getParam('postId')
    this.postController = navigation.getParam('postController')
    const post = this.postController.getPost(id)
    this.setState({ post, id })
  }

  static onPressStar(id, navigation) {
    const onPressFavorite = navigation.getParam('onPressFavorite')
    onPressFavorite(id)
    if (navigation.getParam('isFavorite')) {
      navigation.setParams({ isFavorite: false });
    } else {
      navigation.setParams({ isFavorite: true });
    }
  }

  render() {
    const { post } = this.state
    return (
      <View style={styles.container}>
        <View style={styles.postContent}>
          <Text style={styles.title}>Description</Text>
          {
            post.body ?
              <Text style={styles.postBody}>{post.body}</Text>
              :
              <ActivityIndicator size="large" color="green"></ActivityIndicator>
          }
          <View style={styles.userContent}>
            <Text style={styles.title}>User</Text>
            {
              post.userId &&
              <UserInfo userId={post.userId}></UserInfo>
            }
          </View>
        </View>
        {
          post.id &&
          <Comments
            postId={post.id}></Comments>
        }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  postContent: {
    flexDirection: 'column',
    padding: 15
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 7
  },
  postBody: {
    marginTop: 5
  },
  userContent: {
    marginTop: 20
  }
});