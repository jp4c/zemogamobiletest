import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  ScrollView,
  StatusBar,
  FlatList,
  Text
} from 'react-native';
import { connect } from 'react-redux';
import PostController from '../../controllers/PostController';
import Row from '../Post/Row';
import {
  load_favorites_list,
  load_favorites_posts_ids,
  delete_favorite_post
} from '../../redux/actions/favoritePostsAction'

class Favorites extends Component {

  constructor(props) {
    super(props)
    this.postController = new PostController();
    this.state = {
      favoritesPosts: []
    }
  }

  renderRow = ({ item }) => {
    return (
      <Row
        item={item}
        onPress={this.onPressItem}
        onPressDelete={this.onPressDelete}
      ></Row>
    );
  }

  onPressDelete = id => {
    this.props.deleteFavoritePost(id)
  }


  onPressItem = id => {
    this.postController.posts = this.props.posts
    const post = this.postController.getPost(id)
    this.props.navigation.navigate('PostContent',
      {
        postId: id,
        isFavorite: post.isFavorite,
        postController: this.postController,
        onPressFavorite: this.onPressFavorite
      })
  }

  onPressFavorite = (id) => {
    this.postController.favoritesPosts = this.props.favoritePostsIds
    this.postController.posts = this.props.posts
    this.postController.setFavoritePost(id);
    this.props.setFavoritesPostsIds(this.postController.favoritesPosts, this.postController.posts)
  }

  render() {
    return (
      <ScrollView>
        <StatusBar backgroundColor="green" />
        <View style={styles.container}>
          <FlatList
            data={this.props.favoritesPosts}
            renderItem={this.renderRow}
            keyExtractor={item => "" + item.id}
            extraData={this.props} />
        </View>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const mapStateToProps = (state) => {
  return {
    favoritesPosts: state.favoritesPosts.favoritePosts,
    posts: state.favoritesPosts.posts,
    favoritePostsIds: state.favoritesPosts.favoritePostsIds,
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    setFavoritesPosts: (favorites) => dispatch(load_favorites_list(favorites)),
    setFavoritesPostsIds: (favorites, posts) => dispatch(load_favorites_posts_ids(favorites, posts)),
    deleteFavoritePost: (id) => dispatch(delete_favorite_post(id)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Favorites)