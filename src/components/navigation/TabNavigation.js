import HomeScreen from './Home';
import FavoritesScreen from './Favorites';

import {
  createMaterialTopTabNavigator
} from "react-navigation";
import HeaderBar from './Custom/HeaderBar';

const TabNavigation = createMaterialTopTabNavigator(
  {
    Home: HomeScreen,
    Favorites: FavoritesScreen,
  },
  {
    tabBarOptions: {
      style: {
        backgroundColor: 'green',
      },
      labelStyle: {
        fontSize: 15,
        color: 'white',
        fontWeight: "bold"
      },
    }
  });

export default TabNavigation