export default class CommentController {

  getComments = async postId => {
    try {
      let response = await fetch(`https://jsonplaceholder.typicode.com/posts/${postId}/comments`);
      let comments = await response.json();
      return {
        success: true,
        comments
      }
    } catch (error) {
      return {
        success: false
      }
    }
  }
}