export default class UserController {

  getOneUser = async id => {
    try {
      let response = await fetch("https://jsonplaceholder.typicode.com/users?id=" + id);
      let userJson = await response.json();
      return {
        success: true,
        user: userJson[0]
      }
    } catch (error) {
      return {
        success: false
      }
    }
  }
}