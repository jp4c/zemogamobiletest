import Post from "../models/Post";

export default class PostController {

  constructor() {
    this.posts = []
    this.favoritesPosts = []
  }

  getFavoritePostsInformation() {
    let favoritePostInformation = []
    for (let i = 0; i < this.favoritesPosts.length; i++) {
      for (let j = 0; j < this.posts.length; j++) {
        if (this.favoritesPosts[i] === this.posts[j].id) {
          favoritePostInformation.push(this.posts[j])
        }
      }
    }
    return favoritePostInformation
  }
  getPosts = () => {
    return this.posts
  }

  getPostsFromCloud = async () => {
    try {
      let response = await fetch("https://jsonplaceholder.typicode.com/posts");
      let postsJson = await response.json();
      for (let i = 0; i < postsJson.length; i++) {
        var post = new Post(postsJson[i].userId, postsJson[i].id, postsJson[i].title, postsJson[i].body)
        if (i < 20) {
          post.setRead(false)
        }
        this.posts.push(post)
      }
      return this.posts
    } catch (error) {
      console.error(error);
    }
  }

  setReadPost = id => {
    post = this.posts.find(post => post.id === id);
    if (!post.isRead)
      post.isRead = true
  }

  deletePost = id => {
    let i = 0
    while (i < this.posts.length) {
      if (this.posts[i].id === id) {
        this.posts.splice(i, 1);
        break
      }
      i++
    }
  }

  getFavoritesPosts = () => {
    let favoritePostInformation = []
    for (let i = 0; i < this.favoritesPosts.length; i++) {
      for (let j = 0; j < this.posts.length; j++) {
        if (this.favoritesPosts[i] === this.posts[j].id) {
          favoritePostInformation.push(this.posts[j])
        }
      }
    }
    return favoritePostInformation
  }

  getPost = id => {
    let i = 0
    while (i < this.posts.length) {
      if (this.posts[i].id === id) {
        return this.posts[i]
      }
      i++
    }
  }

  deleteFavoritePost = id => {
    this.favoritesPosts.splice(this.favoritesPosts.indexOf(id), 1);
  }

  setFavoritePost = id => {
    let i = 0
    while (i < this.posts.length) {
      if (this.posts[i].id === id) {
        if (!this.posts[i].isFavorite)
          this.favoritesPosts.push(id)
        else {
          this.favoritesPosts.splice(this.favoritesPosts.indexOf(id), 1);
        }
        this.posts[i].isFavorite = !this.posts[i].isFavorite
      }
      i++
    }
  }
}

