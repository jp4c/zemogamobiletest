/**
 * @format
 */
import React from 'react';
import { AppRegistry } from 'react-native';
import App from './App';
import { name as appName } from './app.json';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import rootReducer from './src/redux/store'
// redux persist
import AsyncStorage from '@react-native-community/async-storage';
import { persistStore, persistReducer } from 'redux-persist'
import { PersistGate } from 'redux-persist/es/integration/react'

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: ['favoritesPosts']
}

const persistedReducer = persistReducer(persistConfig, rootReducer)
const store = createStore(persistedReducer)

const Index = () =>
  <Provider store={store}>
    <PersistGate persistor={persistStore(store)} loading={null}>
      <App />
    </PersistGate>
  </Provider>

AppRegistry.registerComponent(appName, () => Index);
