import {
  createStackNavigator,
  createAppContainer
} from "react-navigation";

import TabNavigation from "./src/components/navigation/TabNavigation";
import PostContent from "./src/components/navigation/PostContent"
import HeaderBar from "./src/components/navigation/Custom/HeaderBar";

const App = createStackNavigator(
  {
    TabNav: {
      screen: TabNavigation,
      navigationOptions: () => ({
        headerBackTitle: null
      }),
    },
    PostContent: {
      screen: PostContent
    }
  },
  {
    defaultNavigationOptions: {
      headerTitle: HeaderBar,
      //title: "Posts",
      headerTitleStyle: { color: 'white', fontSize: 18, },
      headerStyle: { backgroundColor: 'green' },
    }
  });

export default createAppContainer(App);